// SOBRE -> na pratica
$(document).ready(function(){
	$('.carousel-na-pratica').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});
});

// SOBRE -> quem são
$(document).ready(function(){
	$('.carousel-quem-sao').slick({
		infinite: true,
		slidesToShow: 3,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left-2.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-2.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 3,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
				centerMode: true,
			}
		}, {
			breakpoint: 376,
			settings: {
				slidesToShow: 1,
				centerMode: true,
			}
		}
		]
	});
});

// TORNE-SE UM HEROI -> Quem confia em nós
$(document).ready(function(){
	$('.carousel-confia').slick({
		infinite: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left-3.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right-3.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
		responsive: [
		{
			breakpoint: 1025,
			settings: {
				slidesToShow: 2,
			}
		}, {
			breakpoint: 769,
			settings: {
				slidesToShow: 1,
			}
		}, {
			breakpoint: 520,
			settings: {
				slidesToShow: 1,
			}
		}, {
			breakpoint: 376,
			settings: {
				slidesToShow: 1,
			}
		}
		]
	});
});

// TORNE-SE UM HEROI -> benefícios
$(document).ready(function(){
	$('.carousel-beneficios').slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 4000,
		arrows: true,
		dots: false,
		prevArrow:'<img src="imagens/logos/arrow-left.png" class="btn-slick-carousel-left" alt="Anterior" title="Anterior">',
		nextArrow:'<img src="imagens/logos/arrow-right.png" class="btn-slick-carousel-right" alt="Próximo" title="Próximo">',
		pauseOnHover: true,
	});
});

//Iniciar animações
AOS.init({
	disable: 'mobile',
	delay: 0,
	duration: 1200,
	once: true,
});